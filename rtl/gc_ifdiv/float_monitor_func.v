//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   ifdiv
//File Name     :   float_monitor_func.v
//Module name   :   float_monitor_func
//Full name     :   float_monitor_func 
//
//Author        :   xiang tian
//Email         :   
//Data          :   2020/3/7
//Version       :   V1.0
//
//Abstract      :   assemble float number
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//
//
//----------------------------------------------------------------------------- 


function [31:0] float_asmb;
    input        sign;
    input [7:0]  exp;
    input [22:0] man;

    reg   [7:0] exp_int;

    begin
        exp_int = exp + 127;
        float_asmb = {sign,exp_int,man};
    end
    

endfunction
